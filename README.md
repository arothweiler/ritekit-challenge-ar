# ritekit-app


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```


### RITEKIT API PROJECT
-------


At the beginning I had some issues to understand, how application should look alike.
However, I got response from Michal and he explained that I have my freedom. 

Another big issue was CORS. Even thought I never had real experience with REST API's I made small project with Axios.
It kind of helped me that I had small experience and I quickly could create an App. However for some reason the link I used in my previous project
used to work well but the link of RiteKit API don't. I tried to solve that problem and after few hours I found out that the problem was CORS
and it's not my fault but because of the Browser. A quick download solved my issue very fast. 